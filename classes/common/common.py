from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import selenium.common.exceptions as err
import logging
import time
logger = logging.getLogger(__name__)


valid_locators = ['ID', 'NAME', 'XPATH', 'CLASS NAME', 'LINK TEXT']


def click(browser, element, element_type):
    if element_type.upper() in valid_locators:
        WebDriverWait(browser.driver, 5).until(
            EC.presence_of_element_located((element_type.lower(), element)))
        try:
            return browser.driver.find_element(by=element_type.lower(), value=element).click()
        except err.NoSuchElementException:
            if EC.alert_is_present:
                try:
                    logging.info("Handling alert...")
                    alert = browser.driver.switch_to.alert
                    alert.accept()
                    return click(browser=browser, element=element, element_type=element_type)
                except err.NoSuchWindowException:
                    logging.error("Unable to handle alert!")
                    browser.close_browser()
                except err.NoSuchElementException:
                    logging.error("Unable to click")
                    browser.close_browser()
            else:
                logging.info("Element not present")
                return_to_aldeia(browser=browser)
        except err.WebDriverException:
            try:
                logging.info("Element not visible, scrolling into view...")
                element_in_page = browser.driver.find_element(by=element_type.lower(), value=element)
                element_y = element_in_page.location_once_scrolled_into_view['y']
                browser.driver.execute_script('window.scrollTo(0, ' + str(element_y) + ');')
                browser.driver.find_element(by=element_type.lower(), value=element).click()
            except err.WebDriverException:
                logging.info("Element still not in view...")
                return_to_aldeia(browser=browser)


def fill(browser, element, element_type, value):
    if element_type.upper() in valid_locators:
        WebDriverWait(browser.driver, 5).until(
            EC.presence_of_element_located((element_type.lower(), element)))
        try:
            return browser.driver.find_element(by=element_type.lower(), value=element).send_keys(value)
        except err.NoSuchElementException:
            logging.info("Element not found")


def return_to_aldeia(browser):
    logging.info('Returning to aldeia...')
    return click(browser=browser, element_type='XPATH', element='//*[@id="menu_row"]/td[2]/a')


def get_actual_resources(browser):
    resources = {}

    logging.info("Getting resources...")
    resources['wood'] = get_element_value(browser=browser, element_type='ID', element='wood')
    resources['stone'] = get_element_value(browser=browser, element_type='ID', element='stone')
    resources['iron'] = get_element_value(browser=browser, element_type='ID', element='iron')

    logging.info('Wood: {} - Stone: {} - Iron: {}'.format(resources['wood'], resources['stone'], resources['iron']))

    return resources


def get_element_value(browser, element_type, element):
    if element_type.upper() in valid_locators:
        WebDriverWait(browser.driver, 5).until(
            EC.presence_of_element_located((element_type.lower(), element)))
        try:
            return browser.driver.find_element(by=element_type.lower(), value=element).text
        except err.NoSuchElementException:
            print("Element not found")


def get_idle_time(time):
    idle_time = sum([a*b for a, b in zip([3600, 60, 1], map(int, time.split(":")))])
    logging.info("Action will take: " + str(idle_time) + " seconds")
    return idle_time


# def get_all_child(browser, element, element_type):
#
#     if element_type.upper() in valid_locators:
#         WebDriverWait(browser.driver, 5).until(
#             EC.presence_of_element_located((element_type.lower(), element)))
#         parent = browser.driver.find_element(by=element_type.lower(), value=element)
#         try:
#             return parent.find_elements_by_xpath('//map/child::*')
#         except err.NoSuchElementException:
#             print("Element not found")
