import os
import logging.config


basepath = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..'))
logging.config.fileConfig(fname=os.path.join(basepath, 'logging.conf'), disable_existing_loggers=False)
logger = logging.getLogger(__name__)
