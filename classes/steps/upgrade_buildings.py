import classes.pages.main_building as mb
import classes.pages.aldeia as ald
import classes.common.common as cm
import selenium.common.exceptions as err
import logging
logger = logging.getLogger(__name__)


def upgrade_building(browser, structure):
    ald.click_on_structure(browser=browser, structure='main')
    have_resources = cm.get_actual_resources(browser=browser)
    need_resources = mb.get_needed_resources(browser=browser, structure=structure)
    for resource in need_resources.keys():
        if int(have_resources[resource]) < int(need_resources[resource]):
            logging.warning("Not Enough Resources")
            break
        else:
            logging.info("Resource " + resource + " is enough")
    try:
        mb.pg_upgrade_building(browser=browser, structure=structure)
    except err.ElementNotVisibleException:
        logging.warning('Unable to upgrade. Back to aldeia..')
        cm.return_to_aldeia(browser=browser)
    pass



