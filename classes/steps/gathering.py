from classes.common.common import click, return_to_aldeia, get_element_value, get_idle_time
from classes.pages.aldeia import click_on_structure
import classes.pages.place as pl
import logging
logger = logging.getLogger(__name__)

gathering_sizes = ['Pequena', 'Média', 'Grande', 'Extrema']


def gathering(browser, gathering_size):
    if gathering_size in gathering_sizes:
        click_on_structure(browser=browser, structure='place')
        logging.info("Starting Gathering...")
        click(browser=browser, element_type='XPATH',
              element='//*[contains(text(), "Coletando")]')
        pl.pg_gathering(browser=browser, gathering_size=gathering_size)
    else:
        logging.info("Invalid Gathering size input...")
        return return_to_aldeia(browser=browser)
