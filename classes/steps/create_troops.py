import classes.pages.aldeia as aldeia
import classes.pages.barracks as hq
import classes.pages.stable as st
import logging
logger = logging.getLogger(__name__)


def create_troops(browser, structure, troop, quantity=None):
    logging.info('Starting ' + troop + '(s) creation...')
    aldeia.click_on_structure(browser=browser, structure=structure)
    try:
        if structure == 'barracks':
            hq.pg_create_troops(browser=browser, troop=troop, quantity=quantity)
        elif structure == 'stable':
            st.pg_create_troops(browser=browser, troop=troop, quantity=quantity)
    except:
        logging.info("Unable to create troops...")
        return aldeia.return_to_aldeia(browser=browser)
    else:
        logging.info("Invalid structure...")
