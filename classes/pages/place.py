from classes.common.common import click, return_to_aldeia, get_element_value, get_idle_time
from classes.pages.aldeia import click_on_structure
import logging
logger = logging.getLogger(__name__)

valid_troops = ['spear', 'axe', 'sword', 'light', 'heavy']
gathering_sizes = ['Pequena', 'Média', 'Grande', 'Extrema']


def pg_gathering(browser, gathering_size):
    for troop in valid_troops:
        click(browser=browser, element_type='XPATH',
              element='//*[@class="units-entry-all" and @data-unit="' + troop + '"]')
    click(browser=browser, element_type='XPATH',
          element='//div[contains(text(), "' + gathering_size + ' Coleta")]/../div[3]/div/div[2]/a[1]')


def gathering_time(browser, gathering_size):
    if gathering_size in gathering_sizes:
        click_on_structure(browser=browser, structure='place')
        click(browser=browser, element_type='XPATH',
              element='//*[contains(text(), "Coletando")]')
        return get_idle_time(get_element_value(browser=browser, element_type='XPATH',
                                               element='//div[contains(text(), "' + gathering_size + ' Coleta")]/../div[3]/div/ul/li[4]/span[2]'))
    else:
        logging.info("Invalid gathering")
        return return_to_aldeia(browser=browser)
