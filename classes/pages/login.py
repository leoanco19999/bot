from classes.common.common import click, fill
import logging
logger = logging.getLogger(__name__)


class Login:

    def __init__(self, user, password, browser, url=None):
        if url is not None:
            self.url = url
        else:
            self.url = 'https://www.tribalwars.com.br/'
        self.user = user
        self.password = password
        self.browser = browser

    def open_login_page(self):
        logging.info('Logging into game...')
        return self.browser.open_url(self.url)

    def fill_credentials(self):
        logging.info('Logging with user ' + self.user)
        self.browser.driver.find_element_by_id('user').send_keys(self.user)
        self.browser.driver.find_element_by_id('password').send_keys(self.password)
        click(browser=self.browser, element='Login', element_type='LINK TEXT')

    def select_world(self):
        logging.info('Selecting world...')
        click(browser=self.browser, element='world_button_active', element_type='CLASS NAME')

    def login(self):
        self.open_login_page()
        self.fill_credentials()
        self.select_world()
