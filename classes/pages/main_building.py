from classes.common.common import click, get_element_value, get_idle_time
from classes.pages.aldeia import click_on_structure
import logging
logger = logging.getLogger(__name__)

valid_structures = ['main', 'iron', 'wood', 'barracks', 'stable', 'stone', 'wall', 'market', 'farm', 'smith']


def pg_upgrade_building(browser, structure):
    if structure in valid_structures:
        return click(browser=browser,
                     element_type='XPATH',
                     element='//*[@id="main_buildrow_' + structure + '"]/td[7]/a[2]')


def get_needed_resources(browser, structure):
    click_on_structure(browser=browser, structure='main')

    logging.info('Get needed resources...')
    needed_resources = {}

    wood_element = '//*[@id="main_buildrow_' + structure + '"]/td[2]'
    needed_resources['wood'] = get_element_value(browser=browser, element_type='XPATH', element=wood_element)

    stone_element = '//*[@id="main_buildrow_' + structure + '"]/td[3]'
    needed_resources['stone'] = get_element_value(browser=browser, element_type='XPATH', element=stone_element)

    iron_element = '//*[@id="main_buildrow_' + structure + '"]/td[4]'
    needed_resources['iron'] = get_element_value(browser=browser, element_type='XPATH', element=iron_element)

    logging.info('Needed resources - Wood: {} - Stone: {} - Iron: {} '
                 .format(needed_resources['wood'], needed_resources['stone'], needed_resources['iron']))

    return needed_resources


def structure_level(browser, structure):
    click_on_structure(browser=browser, structure='main')
    unformated_level = get_element_value(browser=browser,
                                         element_type='XPATH',
                                         element='//*[@id="main_buildrow_' + structure + '"]/td[1]/span')
    level = unformated_level[6::]
    return int(level)
# re.findall(pattern=re.compile(pattern='(?>\/game)(.*)'), element.get_attribute('href'))[0]


def upgrade_time(browser):
    click_on_structure(browser=browser, structure='main')
    time = get_element_value(browser=browser, element_type='XPATH',
                             element='//*[@id="buildqueue"]/tr[2]/td[2]/span')
    return get_idle_time(time)
