from classes.common.common import click, fill
import logging
logger = logging.getLogger(__name__)

valid_troops = {'explorador': 'spy', 'cavalaria leve': 'light', 'cavalaria pesada': 'heavy'}


def pg_create_troops(browser, troop, quantity=None):
    if troop in valid_troops.keys():
        if quantity is None:
            logging.info('Training max possible ' + troop + '(s)')
            click(browser=browser, element_type='ID', element=valid_troops[troop] + '_0_a')
        else:
            logging.info('Training ' + str(quantity) + troop + '(s)')
            fill(browser=browser, element_type='ID', element=valid_troops[troop] + '_0', value=quantity)
    click(browser=browser, element_type='XPATH', element='//input[@type="submit"]')