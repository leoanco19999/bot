from classes.common.common import click, return_to_aldeia, get_element_value
import logging
logger = logging.getLogger(__name__)

valid_structures = ['main', 'place', 'farm', 'barracks', 'stable']


def click_on_structure(browser, structure):
    if structure in valid_structures:
        return_to_aldeia(browser=browser)
        logging.info('Entering on structure ' + structure)
        click(browser=browser, element_type='XPATH',
              element='//*[@id="show_summary"]/div/div/div[@class="visual-label visual-label-'
                      + structure + ' tooltip-delayed"]/a')


# '//area[@href="/game.php?village=14070&screen=' + structure + '"]'
