from browser.Chrome import Chrome
from classes.pages.login import Login
import classes.common.common as cm
import classes.pages.place as pl
import classes.steps.upgrade_buildings as up
import classes.pages.main_building as mb
import classes.steps.create_troops as ct
import classes.steps.gathering as ga
import classes.pages.aldeia as ald
import logging
import time

logger = logging.getLogger(__name__)


if __name__ == '__main__':

    logger.info('Starting Chrome...')
    chrome = Chrome()

    login = Login(user='TotnotRobot', password='beepboop', browser=chrome)
    login.login()

    while True:
        ga.gathering(browser=chrome, gathering_size='Grande')

        if mb.structure_level(browser=chrome, structure='iron') < 20:
            up.upgrade_building(browser=chrome, structure='iron')
        elif mb.structure_level(browser=chrome, structure='wood') < 20:
            up.upgrade_building(browser=chrome, structure='wood')
        else:
            up.upgrade_building(browser=chrome, structure='stone')

        time.sleep(pl.gathering_time(browser=chrome, gathering_size='Grande'))
    # logging.info('Finishing...\n\n')
