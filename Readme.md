# Todo

- ~~Implement click with **wait** - use try catch~~
- ~~Create page for 'aldeia'~~
- ~~Create page for 'edificio_principal'~~
- ~~Create page for 'barracks'~~
- ~~Method for next available action w/ time sleep~~
- Create page for each building
- Implement farming
- Handle all the possible errors 
    - Try-Exception and Logging
- ~~implement "gathering"~~

# To Fix
- ~~fix "see_structure_level" method~~
- ~~Make the 'click' method scroll until the element is in view~~
- Make pages do not depend on importing browser
    - use inheritance
- ~~Implement logging~~
