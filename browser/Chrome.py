import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class Chrome:

    def __init__(self):
        self.options = Options()
        self.options.add_argument('--incognito')
        #self.options.add_argument('--headless')
        #self.options.add_argument('--disable-gpu')
        if os.name == 'nt':
            self.driver_location = os.path.join(os.path.dirname(__file__), os.path.join('driver', 'chromedriver.exe'))
        else:
            self.driver_location = os.path.join(os.path.dirname(__file__), os.path.join('driver', 'chromedriver'))
        self.driver = webdriver.Chrome(executable_path=self.driver_location, chrome_options=self.options)
        if os.name == 'nt':
            self.driver.maximize_window()

    def open_url(self, url):
        return self.driver.get(url)

    def refresh_page(self):
        return self.driver.refresh()

    def close_browser(self):
        return self.driver.quit()
